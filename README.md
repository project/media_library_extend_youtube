INTRODUCTION
------------

The Media Library Youtube module provides a plugin for
[Media Library Extend](https://www.drupal.org/project/media_library_extend)
that integrates with the Youtube API to list a channel's videos and create
media entities from them.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/media_library_extend_youtube

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/media_library_extend_youtube

REQUIREMENTS
------------

This module requires the following modules:

 * [Media Library Extend](https://www.drupal.org/project/media_library_extend)

RECOMMENDED MODULES
-------------------

 * [Media Library Youtube](https://www.drupal.org/project/media_library_extend_youtube)

INSTALLATION
------------

 * Install as you would normally [install a contributed Drupal module](https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules).

CONFIGURATION
-------------

 * Configure available Media Library Panes in Administration » Configuration »
   Media » Media library » Panes

   The Youtube plugin is available for Media bundles with source types Generic,
   Remote video and Video embed field.

   You will need to supply a Google server API key with the Youtube Data API v3
   enabled. You will also need the channel IDs of any channel you want to be
   able to browse. You can get the channel ID by visiting the channel's page and
   copying the part after `https://www.youtube.com/channel/`.

MAINTAINERS
-----------

Current Maintainers:
 * [David I. (Mirroar)](https://www.drupal.org/u/mirroar)

This project has been sponsored by:
 * [werk21](https://www.werk21.de)

   werk21 is based in Berlin, Germany and specializes in the development of
   custom web applications using Drupal. Our 20 staff members care for over 300
   clients in the range of politics and NGO.
