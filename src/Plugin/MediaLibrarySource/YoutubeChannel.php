<?php

namespace Drupal\media_library_extend_youtube\Plugin\MediaLibrarySource;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Utility\Token;
use Drupal\media_library_extend\Plugin\MediaLibrarySource\MediaLibrarySourceBase;
use GuzzleHttp\Client;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a media library pane to pull placeholder images from lorem.picsum.
 *
 * @MediaLibrarySource(
 *   id = "youtube_channel",
 *   label = @Translation("Youtube Channel"),
 *   source_types = {
 *     "generic",
 *     "oembed:video",
 *     "video_embed_field"
 *   },
 * )
 */
class YoutubeChannel extends MediaLibrarySourceBase {

  /**
   * The http client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('token'),
      $container->get('file_system'),
      $container->get('http_client')
    );
  }

  /**
   * Constructs a new LoremPicsum object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Utility\Token $token
   *   The token service.
   * @param \GuzzleHttp\Client $http_client
   *   The HTTP client.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, Token $token, FileSystemInterface $file_system, Client $http_client) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $token, $file_system);
    $this->httpClient = $http_client;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'api_key' => '',
      'channels' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['api_key'] = [
      '#title' => $this->t('API Key'),
      '#description' => $this->t('To use this plugin you need a valid google API server key with the Youtube Data API v3 enabled.'),
      '#type' => 'textfield',
      '#default_value' => $this->configuration['api_key'],
      '#required' => TRUE,
    ];

    // @todo Validate channel IDs using Youtube API.
    $form['channels'] = [
      '#title' => $this->t('Available channels'),
      '#description' => $this->t("Enter the IDs of channels you would like to add videos from, one channel per line. You can get the channel ID by visiting the channel's page and copying the part after <code>https://www.youtube.com/channel/</code>."),
      '#type' => 'textarea',
      '#default_value' => $this->configuration['channels'],
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getCount() {
    $videos = $this->queryResults();
    return $videos['pageInfo']['totalResults'];
  }

  /**
   * {@inheritdoc}
   */
  public function getResults() {
    $videos = $this->queryResults();

    $results = [];
    foreach ($videos['items'] as $video) {
      $results[] = [
        'id' => $video['id']['videoId'],
        'label' => $video['snippet']['title'],
        'preview' => [
          '#type' => 'html_tag',
          '#tag' => 'img',
          '#attributes' => [
            'src' => $video['snippet']['thumbnails']['medium']['url'],
            'alt' => $video['snippet']['title'],
            'title' => $video['snippet']['title'],
          ],
        ],
      ];
    }

    return $results;
  }

  /**
   * Query the youtube for current results and cache them.
   *
   * @return array
   *   The current set of result data.
   */
  protected function queryResults() {
    $page = $this->getValue('page');
    $options = [
      'count' => $this->configuration['items_per_page'],
      'offset' => $this->configuration['items_per_page'] * $page,
    ];
    $filter_options = serialize($options + [
      'query' => $this->getValue('query'),
      'channel' => $this->getValue('channel'),
    ]);

    if (!isset($this->cachedResults) || $this->cachedOptions != $filter_options) {
      $this->cachedOptions = $filter_options;

      $response = $this->httpClient->request('GET', 'https://www.googleapis.com/youtube/v3/search', [
        'headers' => [
            // @todo Check if we need this header.
          'User-Agent' => 'Mozilla/5.0',
        ],
        'query' => [
          'channelId' => $this->getSelectedChannelId(),
          'maxResults' => $this->configuration['items_per_page'],
          'order' => 'date',
          'type' => 'video',
          'videoEmbeddable' => 'true',
          'part' => 'snippet',
          'key' => $this->configuration['api_key'],
          'q' => $this->getValue('query'),
          // @todo Add pagination with a cached set of page tokens.
          // 'pageToken' => $this->getPageToken($this->getValue('page')),
        ],
      ]);

      // @todo Error handling.
      $this->cachedResults = json_decode((string) $response->getBody(), TRUE);
    }

    return $this->cachedResults;
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array &$form, FormStateInterface $form_state) {
    $form['query'] = [
      '#title' => t('Search query'),
      '#type' => 'textfield',
    ];

    $form['channel'] = [
      '#title' => t('Channel'),
      '#type' => 'select',
      '#options' => $this->getChannelOptions(),
      '#default_value' => $this->getSelectedChannelId(),
    ];

    return $form;
  }

  /**
   * Gets a list of channels and their names.
   */
  protected function getChannelOptions() {
    $channel_ids = array_map('trim', explode("\n", $this->configuration['channels']));

    $response = $this->httpClient->request('GET', 'https://www.googleapis.com/youtube/v3/channels', [
      'headers' => [
          // @todo Check if we need this header.
        'User-Agent' => 'Mozilla/5.0',
      ],
      'query' => [
        'id' => implode(',', $channel_ids),
        'part' => 'snippet',
        'key' => $this->configuration['api_key'],
        // @todo If for some reason we want to support more than 50 channels,
        // we need to add pagination here.
        'maxResults' => 50,
      ],
    ]);

    // @todo Error handling.
    $options = [];
    $channels = json_decode((string) $response->getBody(), TRUE);
    foreach ($channels['items'] as $channel) {
      $options[$channel['id']] = $channel['snippet']['title'];
    }

    // @todo Sort channels like the user entered them in config.
    return $options;
  }

  /**
   * Gets the id of the currently selected channel.
   */
  protected function getSelectedChannelId() {
    $selected = $this->getValue('channel');

    if ($selected) {
      return $selected;
    }

    // Fall back to first channel.
    $channel_ids = array_map('trim', explode("\n", $this->configuration['channels']));
    return $channel_ids[0];
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityId($selected_id) {
    $response = $this->httpClient->request('GET', 'https://www.googleapis.com/youtube/v3/videos', [
      'headers' => [
          // @todo Check if we need this header.
        'User-Agent' => 'Mozilla/5.0',
      ],
      'query' => [
        'id' => $selected_id,
        'part' => 'snippet',
        'key' => $this->configuration['api_key'],
      ],
    ]);

    // @todo Error handling.
    $videos = json_decode((string) $response->getBody(), TRUE);
    $video = $videos['items'][0];

    $entity = $this->createEntityStub($video['snippet']['title']);
    $source_field = $this->getSourceField();
    $entity->{$source_field} = 'https://www.youtube.com/watch?v=' . $selected_id;
    $entity->save();

    return $entity->id();
  }

}
